import cv2, numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import math, sys



class ImageProcessing():
    # сглаживания с помощью медианного, равномерного фильтра и фильтра Гаусса
    def smoothing(images, method, size=3):
        try:
            size = float(size)
        except:
            size = 0
        # фильтр работает только для черно-белых фото, поэтому надо разделить на каналы 
        if method == "медианный фильтр":
            _images = images.copy()
            if size > 1:
                for i in _images:
                    for rgb in range(3):
                        i[:,:,rgb] = ndimage.grey_erosion(i[:,:,rgb], size=(15,15))

            return _images
        elif method == "фильтр Гаусса":
            _images = images.copy()
            if size > 0:
                for i in _images:
                    for rgb in range(3):
                        i[:,:,rgb] = ndimage.gaussian_filter(i[:,:,rgb], size)

            return _images
        elif method == "равномерный фильтр":
            _images = images.copy()
            if size > 0:
                for i in _images:
                    for rgb in range(3):
                        i[:,:,rgb] = ndimage.uniform_filter(i[:,:,rgb], size)

            return _images
        else:
            return images
    
    # повышение резкости фото .
    def definition(images, method):
        if method != "нет": 
            _images = images.copy()
            # ядра для повышения качества
            low = np.array([[0, -1, 0], [-1, 5, -1.0], [0, -1, 0]])
            medium = np.array([[-2, -1, 0], [-1, 1, 1], [0, 1, 2]])
            hight = np.array([[1, 8, -1], [-1, -2, -1], [-1, -1, -1]])

            kernels = {"слабая резкость": low, "средняя резкость": medium, "высокая резкость": hight}
            kernel = kernels[method]
            for i in _images:
                for rgb in range(3):
                    i[:,:,rgb] = ndimage.convolve(i[:,:,rgb], kernel, mode='reflect')

            return _images
        else:
            return images

    # изменение размера фото
    def photoSize(images, x, y):
        x = int(x) if x.isdigit() else 0
        y = int(y) if y.isdigit() else 0
        
        if x == 0 and y == 0:
            return images
        else:
            _images = []
            for i in images:
                if x == 0:
                    _images.append(cv2.resize(i, dsize=(i.shape[0], y), interpolation=cv2.INTER_LINEAR))
                elif y == 0:
                    _images.append(cv2.resize(i, dsize=(x, i.shape[1]), interpolation=cv2.INTER_LINEAR))
                else:
                    _images.append(cv2.resize(i, dsize=(x, y), interpolation=cv2.INTER_LINEAR))
            return np.array(_images)

    def rotatePhoto(images, angle):
        if int(angle) != 0:
            angle = -int(angle)
            _images = []
            for i in images:
                _images.append(ndimage.rotate(i, angle))
            return np.array(_images)
        else:
            return images
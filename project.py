from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QRegion
from Ui_main import Ui_MainWindow
from Ui_secondWindow import Ui_InfoWindow

from backend import *

class mywindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(mywindow,self).__init__()
        self.setupUi(self)
        self.directory = [] # массив с путями фотографий
        self.images = np.array([]) # массив фотографий
        self.btnCloseWindow.clicked.connect(self.closeWindow)
        self.btnMinWindow.clicked.connect(self.minimizeWindow)
        self.btnOpen.clicked.connect(self.browse_folder)
        self.btnSave.clicked.connect(self.savePhoto)
        self.btnInfo.clicked.connect(self.infoAbProg)
        self.btnExit.clicked.connect(self.closeWindow)
        self.btnShowPhoto.clicked.connect(self.showPhoto)
        self.btnReset.clicked.connect(self.resetSizePhoto)
        self.horizontalSlider.valueChanged.connect(self.updateValueAngle)

        ################################################################
        """скрыть MainWindow"""
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)

        radius = 15
        base = self.rect()
        ellipse = QtCore.QRect(0, 0, 2 * radius , 2 * radius)

        base_region = QRegion(base.adjusted(radius, 0, -radius, 0))
        base_region |= QRegion(base.adjusted(0, radius, 0, -radius))
        
        base_region |= QRegion(ellipse, QRegion.Ellipse)
        ellipse.moveTopRight(base.topRight())
        base_region |= QRegion(ellipse, QRegion.Ellipse)
        ellipse.moveBottomRight(base.bottomRight())
        base_region |= QRegion(ellipse, QRegion.Ellipse)
        ellipse.moveBottomLeft(base.bottomLeft())
        base_region |= QRegion(ellipse, QRegion.Ellipse)

        self.setMask(base_region)
        #################################################################
        self.btnUpBar.mousePressEvent = self.my_mousePressEvent
        self.btnUpBar.mouseMoveEvent = self.my_moveWindow

        self.numPhoto.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.comboBox_1.setEditable(True)
        self.comboBox_1.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
        self.comboBox_2.setEditable(True)
        self.comboBox_2.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
        self.comboBox_3.setEditable(True)
        self.comboBox_3.lineEdit().setAlignment(QtCore.Qt.AlignCenter)

        # в QLineEdit в который записываем коэф. может быть записано только положительное число
        reg_ex = QtCore.QRegExp("^[0-9]*[.,]?[0-9]+$")
        input_validator = QtGui.QRegExpValidator(reg_ex, self.valueKoef_1)
        self.valueKoef_1.setValidator(input_validator)

        # в QLineEdit в который записываем высоту может быть записано только целое число
        reg_ex = QtCore.QRegExp("^[0-9]+$")
        input_validator = QtGui.QRegExpValidator(reg_ex, self.valueHight)
        self.valueHight.setValidator(input_validator)
        # в QLineEdit в который записываем ширину может быть записано только целое число
        reg_ex = QtCore.QRegExp("^[0-9]+$")
        input_validator = QtGui.QRegExpValidator(reg_ex, self.valueWeight)
        self.valueWeight.setValidator(input_validator)


    # полуения путей снимков, сохранение этих путей(в directory)
    def browse_folder(self):
        self.listWidget.clear()
        self.directory = QtWidgets.QFileDialog.getOpenFileNames(self, None, None, "*.png *.jpg *.jpeg")[0]
        if self.directory:
            # выводим путь каждой фотографии
            self.listWidget.addItems(self.directory)
            # выводим количество фото
            if len(self.directory)%10 == 1:
                self.numPhoto.setText(f"Выбрано {len(self.directory)} изображениe!")
            elif len(self.directory)%10 >= 2 and len(self.directory)%10 <=4:
                self.numPhoto.setText(f"Выбрано {len(self.directory)} изображения!")
            else:
                self.numPhoto.setText(f"Выбрано {len(self.directory)} изображений!")
        else:
            self.images = np.array([])
            self.numPhoto.setText(f"Выбрано 0 изображений!")

    def showPhoto(self):
        # сохраняем фотографии в массив
        self.images = np.array([np.array(plt.imread(i)) for i in self.directory])
        new_images = ImageProcessing.smoothing(self.images, self.comboBox_1.currentText(), self.valueKoef_1.text())
        new_images = ImageProcessing.definition(new_images, self.comboBox_2.currentText())
        new_images = ImageProcessing.photoSize(new_images, x=self.valueWeight.text(), y=self.valueHight.text())
        new_images = ImageProcessing.rotatePhoto(new_images, self.horizontalSlider.value())
        if len(new_images)>0:
            plt.rcParams['toolbar'] = 'None'
            fig = plt.figure(" ", figsize=(8, 8))
            fig.patch.set_facecolor(color="#221632")
            size_dir = len(new_images)
            rows = columns = math.ceil(size_dir**0.5)
            for i in range(1, size_dir + 1):
                fig.add_subplot(rows, columns, i)
                plt.imshow(new_images[i-1])
                plt.axis('off')
            plt.show()
        else:
            self.msgError("Фотографии не выбраны!")

    def closeWindow(self):
        self.close()

    def minimizeWindow(self):
        self.showMinimized()
    
    def resetSizePhoto(self):
        self.valueHight.setText('исходная')
        self.valueWeight.setText('исходная')

    def updateValueAngle(self, value):
        self.valueAngle.setText(str(value) + "°")

    def my_mousePressEvent(self, event):
        self.dragPos = event.globalPos()
        
    def my_moveWindow(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(self.pos() + event.globalPos() - self.dragPos)
            self.dragPos = event.globalPos()
            event.accept()  
    
    def savePhoto(self):
        # сохраняем фотографии в массив
        self.images = np.array([np.array(plt.imread(i)) for i in self.directory])

        # директория куда сохраним все пробразованные изображения\
        directoryTo = QtWidgets.QFileDialog.getExistingDirectory(self) + '/'

        new_images = ImageProcessing.smoothing(self.images, self.comboBox_1.currentText(), self.valueKoef_1.text())
        new_images = ImageProcessing.definition(new_images, self.comboBox_2.currentText())
        new_images = ImageProcessing.photoSize(new_images, x=self.valueWeight.text(), y=self.valueHight.text())
        new_images = ImageProcessing.rotatePhoto(new_images, self.horizontalSlider.value())
        for num, i in enumerate(new_images, start=0):
            if self.comboBox_3.currentText() != 'исходный':
                namePhoto = self.directory[num].split('/')[-1].split('.')[0] +'.' + self.comboBox_3.currentText()
                cv2.imwrite(directoryTo + namePhoto, i)
            else:
                cv2.imwrite(directoryTo+self.directory[num].split('/')[-1], i)
                print(directoryTo+self.directory[num].split('/')[-1])

    def infoAbProg(self):
        global SecondWindow # чтобы сразу окно не закрывалось
        SecondWindow = QtWidgets.QMainWindow()
        ui = Ui_InfoWindow()
        ui.setupUi(SecondWindow)
        self.hide()

        SecondWindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        SecondWindow.setStyleSheet(" background-color:rgb(34, 22, 50); border : 4px solid; border-color:rgb(91, 4, 188); border-radius: 7px")
        
        def closeSecondWindow():
            self.show()
            SecondWindow.close()

        def my_mousePressEvent(event):
            SecondWindow.dragPos = event.globalPos()
        
        def my_moveWindow(event):
            if event.buttons() == QtCore.Qt.LeftButton:
                SecondWindow.move(SecondWindow.pos() + event.globalPos() - SecondWindow.dragPos)
                SecondWindow.dragPos = event.globalPos()
                event.accept()  

        ui.btnOk_SecWin.clicked.connect(closeSecondWindow)
        ui.text_SecWin.mousePressEvent = my_mousePressEvent
        ui.text_SecWin.mouseMoveEvent = my_moveWindow
        
        SecondWindow.show()

    @staticmethod
    def msgError(text):
        global msgBox
        msgBox = QtWidgets.QMessageBox()
        msgBox.setText(text)
        msgBox.setWindowTitle("Ошибка!")
        msgBox.setWindowFlags(QtCore.Qt.FramelessWindowHint) 
        msgBox.setStyleSheet("""
        QMessageBox {background-color:rgb(34, 22, 50); border : 4px solid; border-color:rgb(91, 4, 188); border-radius: 7px; }
        QPushButton { background-color:rgb(91, 4, 188); font-family: Georgia; color: white; border: none;  border-radius:5px }
        QPushButton:hover{background-color:rgb(121, 62, 188)}
        QLabel {font-family:Georgia; color: white; text-align: center; border: none}
        """)
        buttonAceptar  = msgBox.addButton("OK", msgBox.close())
        buttonAceptar.setMinimumSize(QtCore.QSize(50, 20))
        msgBox.show()    


app = QtWidgets.QApplication(sys.argv)
window = mywindow()
window.setWindowFlags(QtCore.Qt.FramelessWindowHint) 
window.show()
sys.exit(app.exec_())

